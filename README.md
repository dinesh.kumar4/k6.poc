# K6 API Performance Testing Framework

POC for performance tests using K6 performance tool.

https://k6.io

### K6 Feature
Tests are written in javascript.
It can be successfully integrate into CI/CD automation pipelines providing a much needed feature.

### To Run Tests
Pre-requisite - K6 app should be installed on the machine for running tests or docker can be used.

`npm test:performance` to run tests

### Test Report
By default, k6 will print runtime information and general results to `stdout` while the test is running, and a summary after the test has ended.
#### Two types of validations are in place for this template:
Checks - Checks are like asserts but differ in that they don't pass or fail the builds, instead they just store the result of the check, pass or fail, and let the script execution continue

Thresholds -  Thresholds are hard limits and if these thresholds are exceeded in test run, K6 genrates non-zero exit code resulting in failing the build.
Also if user needs, the execution can be halted if a threshold breaks during test run.


#### Results in Grafana Dashboard 
InfluxDB/Grafana results config is included in this POC
*Pre-Requisites - You need to have pres-configued Grafana dashboard with influxdb as source.
More info - https://docs.k6.io/docs/influxdb-grafana

