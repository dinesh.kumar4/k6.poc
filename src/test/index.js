import { group } from 'k6';
import options from '../utils/test-options.js';
import auth from '../utils/auth.js';
import { profileInstance } from './profile.api';

export { options };
export function setup() {
  const res = auth().json();
  const tokenType = res.token_type;
  const accessToken = res.access_token;
  const params = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `${tokenType} ${accessToken}`,
    },
  };
  return params;
}
export default (data) => {
  group('First Suite', () => {
    profileInstance.test(data);
  });
};
