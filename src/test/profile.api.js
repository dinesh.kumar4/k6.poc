import { group } from 'k6';
import http from 'k6/http';
import { config } from '../../config.js';
import BaseTest from '../helpers/BaseTest.js';

// const saleRequest = JSON.parse(open('../json-data/sale-request.json'));

class customerProfileApi extends BaseTest {
  constructor() {
    super('Customer Profile Api Tests');
    this.URL = `${config.BASE_URL}/health`;
  }
  test() {
    group(this.TEST_NAME, () => {
      const response = http.get(this.URL);
      this.checkResponse(response);
    });
  }
}

export const profileInstance = new customerProfileApi();