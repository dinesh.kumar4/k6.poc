import http from 'k6/http';
import { config } from '../../config.js';

export default () => {
  const authHeader = {
    'Content-Type': 'application/x-www-form-urlencoded',
    Authorization: __ENV.CLIENT_AUTH,
  };
  const authFormdata = {
    grant_type: config.TOKEN_GRANT_TYPE,
    scope: config.SCOPES,
  };
  return http.post(config.AUTH_URL, authFormdata, { headers: authHeader });
}
