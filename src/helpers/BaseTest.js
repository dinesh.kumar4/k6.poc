import { check } from 'k6';
import { Rate } from 'k6/metrics';
import testChecks from './test-checks.js';
import auth from './auth.js';
import { config } from '../../config.js'

export default class BaseTest {
  constructor(TEST_NAME) {
    this.TEST_NAME = TEST_NAME;
    const perfThreshold = `${this.TEST_NAME}-> Performance Threshold`
    const errorThreshold = `${this.TEST_NAME} -> Failed Requests`
    this.thresholds = {
      [perfThreshold]: [`rate>${config.PERFORMANCE_PERCENTAGE_THRESHOLD / 100}`],
      [errorThreshold]: [{
        threshold: `rate<${config.ERROR_PERCENTAGE_THRESHOLD / 100}`,
        abortonFail:true
      }],
    };
    this.perfThresholdMeasure = new Rate(perfThreshold);
    this.errorThresholdMeasure = new Rate(errorThreshold);
  }
  checkResponse(response) {
    check(
      response, Object.assign(
        {},
        testChecks.statusOk(response, this.errorThresholdMeasure),
        testChecks.transactionTimeOk,
        testChecks.servicePerformanceOk(response, this.perfThresholdMeasure)
      ),
    );
  }
}
