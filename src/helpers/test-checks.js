import { config } from '../../config.js';

export default {
  statusOk:  (response, threshold) => { 
    const result = [200, 201, 202, 203, 204].includes(response.status);
    
    threshold.add(!result);
    return {
      'Request status should be 2xx (success)': () => result,
    }
  },
  transactionTimeOk: {
    [`Transaction Time should be <${config.EXPECTED_TRANSACTION_TIME}ms`]: response =>
      response.timings.duration < config.EXPECTED_TRANSACTION_TIME,
  },
  servicePerformanceOk: (response, threshold) => {
    const performanceComparison = response.timings.waiting < config.EXPECTED_SERVER_PROCESSING_TIME * 1.1;
    threshold.add(performanceComparison);
    return {
      [`Expected server response time is ${config.EXPECTED_SERVER_PROCESSING_TIME}ms. Latest results should not be 10% above.`]: () =>
        performanceComparison,
    };
  },
};
