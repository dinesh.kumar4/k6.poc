import { profileInstance } from '../test/customer-profile-api-tests.js';
import { config } from '../../config.js'

const options = {
  duration: `${config.TEST_DURATION_SECONDS}s`,
  vus: config.VIRTUAL_USERS,
  rps: config.REQUESTS_PER_SECONDS,
  thresholds: Object.assign(
    {},
    profileInstance.thresholds
  ),
  noConnectionReuse: false,
  userAgent: 'zip-qe/performance-test',
};
export default options;
