export const config = {
  /* Base Url for Api Under Test */

  BASE_URL: 'https://api-customerprofile.internal.sand.au.edge.zip.co',
  
  /** Auth Api Details */
  
  AUTH_URL: 'https://sandbox.zip.co/v2/connect/token',
  TOKEN_GRANT_TYPE: '',
  SCOPES: '',
  
  /** Expected Transaction and Server Times */

  EXPECTED_TRANSACTION_TIME: 200,         // Including Request Sending, Server Processing and Response Receiving times
  EXPECTED_SERVER_PROCESSING_TIME: 170,   // Pure Server Processing Time
  
  // Thresholds => The build will fail or pass based on these 

  PERFORMANCE_PERCENTAGE_THRESHOLD: 95,   // Percent of transactions (Server Response Time) should be within the sepcified range
  ERROR_PERCENTAGE_THRESHOLD: 5,          // Error percentage we can tolerate, Test will be terminated if this threshold breaks
  
  /** Performance Test Management */
  
  TEST_DURATION_SECONDS: 5,
  VIRTUAL_USERS: 1,
  REQUESTS_PER_SECONDS: 1
};
